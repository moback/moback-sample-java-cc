package com.moback.cc;

import com.moback.custom.IExecute;
import com.moback.custom.model.CustomResponse;

public class HelloWorld implements IExecute<HelloCustomRequest> {

	public CustomResponse execute(HelloCustomRequest customRequset) {
		HelloCustomResponse response = new HelloCustomResponse();
		response.message = "Hello, " + customRequset.name;
		return response;
	}

}